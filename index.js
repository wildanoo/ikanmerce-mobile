/** @format */

import {AppRegistry} from 'react-native';
import ParentStack from './navigations/ParentStack.js';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => ParentStack);
