import { createStackNavigator } from "react-navigation";
import App from '../pages/App'

export default createStackNavigator({
  App: { screen: App },
});
